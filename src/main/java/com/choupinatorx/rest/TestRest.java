package com.choupinatorx.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

@Path("/test")
public class TestRest extends Application {

	@GET
    @Path("/isUp")
    public Response ping() {
        return Response.ok().entity("Service online").build();
    }
	
}